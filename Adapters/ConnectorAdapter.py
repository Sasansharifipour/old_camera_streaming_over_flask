import logging

class ConnectorAdapter():

    def __init__(self, device_name, device_start_func, device_stop_func):
        super().__init__()
        self.__device_start_func = device_start_func 
        self.__device_stop_func = device_stop_func
        self.__device_name = device_name

    def start(self):
        try:
            self.__device_start_func()
        except Exception as e:
            logging.error(f"Error starting {self.__device_name} connector: {e}")

    def stop(self):
        try:
            self.__device_stop_func()
        except Exception as e:
            logging.error(f"Error stopping {self.__device_name} connector: {e}")