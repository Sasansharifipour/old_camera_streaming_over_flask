import threading
import serial
import serial.tools.list_ports
import time
from datetime import datetime
import struct
import os
import numpy as np
import logging

class Radar_IWR6843_Connector():

    def __init__(self, get_data_func= None):
        self.__get_data_func= get_data_func
        self.__current_dir = os.path.dirname(os.path.abspath(__file__))
        self.__configFileName = os.path.join(self.__current_dir, 'iwr6843_config.cfg')
        self.__CLIport = None
        self.__Dataport = None
        self.__HeaderLength = 40
        self.__DetectSerialPorts = True
        self.__logger = logging.getLogger(__name__)
        self.__startDevice = False

    def __getDefaultPorts(self):
        CLIPortName = ""
        DataPortName = ""
        cliPortDescription = "UART Bridge: Enhanced COM Port"
        dataPortDescription = "UART Bridge: Standard COM Port"

        ports = serial.tools.list_ports.comports()

        for port, desc, hwid in sorted(ports):
            if cliPortDescription in desc:
                CLIPortName = port
            elif dataPortDescription in desc:
                DataPortName = port

        return CLIPortName, DataPortName

    def __serialConfig(self):
        try:
            CLIPortName, DataPortName = "", ""
            if self.__DetectSerialPorts:
                CLIPortName, DataPortName = self.__getDefaultPorts()

            self.__CLIport = serial.Serial(CLIPortName, 115200)
            self.__Dataport = serial.Serial(DataPortName, 921600)

            config = [line.rstrip('\r\n') for line in open(self.__configFileName)]
            for line in config:
                self.__CLIport.write((line + '\n').encode())
                time.sleep(0.01)

            return True
        except Exception as e:
            self.__logger.error(f"Error in serial configuration: {e}")
            return False

    def __readDataFromBuffer(self, readCount):
        try:
            while True:
                if self.__Dataport.in_waiting >= readCount:
                    readBuffer = self.__Dataport.read(readCount)
                    byteVec = np.frombuffer(readBuffer, dtype='uint8')
                    return byteVec
        except Exception as e:
            self.__logger.error(f"Error reading data from buffer: {e}")
            return None

    def __readHeader(self):
        return self.__readDataFromBuffer(readCount=self.__HeaderLength)

    def __readPacketBody(self, PacketLength):
        return self.__readDataFromBuffer(readCount=PacketLength - self.__HeaderLength)

    def __ParseHeader(self, byteBuffer):
        vitalsign = {}

        idX = 0
        vitalsign["magicWord"] = str(byteBuffer[idX:idX + 8])
        idX += 8
        vitalsign["version"] = format(int.from_bytes(byteBuffer[idX:idX + 4], byteorder='little'), 'x')
        idX += 4
        vitalsign["totalPacketLen"] = int.from_bytes(byteBuffer[idX:idX + 4], byteorder='little')
        idX += 4
        vitalsign["platform"] = format(int.from_bytes(byteBuffer[idX:idX + 4], byteorder='little'), 'x')
        idX += 4
        vitalsign["frameNumber"] = int.from_bytes(byteBuffer[idX:idX + 4], byteorder='little')
        idX += 4
        vitalsign["timeCpuCycles"] = int.from_bytes(byteBuffer[idX:idX + 4], byteorder='little')
        idX += 4
        vitalsign["numDetectedObj"] = int.from_bytes(byteBuffer[idX:idX + 4], byteorder='little')
        idX += 4
        vitalsign["numTLVs"] = int.from_bytes(byteBuffer[idX:idX + 4], byteorder='little')
        idX += 4
        vitalsign["header_reserved"] = int.from_bytes(byteBuffer[idX:idX + 4], byteorder='little')
        
        return vitalsign

    def __ParsePacketBody(self, vitalsign, byteBuffer):
        
        idX = 0
        vitalsign["tlv1_type_"] = int.from_bytes(byteBuffer[idX:idX + 4], byteorder='little')
        idX += 4
        vitalsign["tlv1_length_"] = int.from_bytes(byteBuffer[idX:idX + 4], byteorder='little')
        idX += 4
        vitalsign["rangeBinIndexMax"] = int.from_bytes(byteBuffer[idX:idX + 2], byteorder='little')
        idX += 2
        vitalsign["rangeBinIndexPhase"] = int.from_bytes(byteBuffer[idX:idX + 2], byteorder='little')
        idX += 2
        vitalsign["maxVal"] = struct.unpack('<f', byteBuffer[idX:idX + 4])[0]
        idX += 4
        vitalsign["processingCyclesOut"] = int.from_bytes(byteBuffer[idX:idX + 4], byteorder='little')
        idX += 4
        vitalsign["rangeBinStartIndex"] = int.from_bytes(byteBuffer[idX:idX + 2], byteorder='little')
        idX += 2
        vitalsign["rangeBinEndIndex"] = int.from_bytes(byteBuffer[idX:idX + 2], byteorder='little')
        idX += 2
        vitalsign["unwrapPhasePeak_mm"] = str(byteBuffer[idX:idX + 4].view(dtype=np.float32)[0])
        idX += 4
        vitalsign["outputFilterBreathOut"] = struct.unpack('<f', byteBuffer[idX:idX + 4])[0]
        idX += 4
        vitalsign["outputFilterHeartOut"] = struct.unpack('<f', byteBuffer[idX:idX + 4])[0]
        idX += 4
        vitalsign["heartRateEst_FFT"] = struct.unpack('<f', byteBuffer[idX:idX + 4])[0]
        idX += 4
        vitalsign["heartRateEst_FFT_4Hz"] = struct.unpack('<f', byteBuffer[idX:idX + 4])[0] / 2
        idX += 4
        vitalsign["heartRateEst_xCorr"] = struct.unpack('<f', byteBuffer[idX:idX + 4])[0]
        idX += 4
        vitalsign["heartRateEst_peakCount"] = struct.unpack('<f', byteBuffer[idX:idX + 4])[0] # zero always
        idX += 4
        vitalsign["breathingRateEst_FFT"] = struct.unpack('<f', byteBuffer[idX:idX + 4])[0]
        idX += 4
        vitalsign["breathingRateEst_xCorr"] = struct.unpack('<f', byteBuffer[idX:idX + 4])[0]
        idX += 4
        vitalsign["breathingRateEst_peakCount"] = struct.unpack('<f', byteBuffer[idX:idX + 4])[0]
        idX += 4
        vitalsign["confidenceMetricBreathOut"] = struct.unpack('<f', byteBuffer[idX:idX + 4])[0]
        idX += 4
        vitalsign["confidenceMetricBreathOut_xCorr"] = struct.unpack('<f', byteBuffer[idX:idX + 4])[0]
        idX += 4
        vitalsign["confidenceMetricHeartOut"] = struct.unpack('<f', byteBuffer[idX:idX + 4])[0]
        idX += 4
        vitalsign["confidenceMetricHeartOut_4Hz"] = struct.unpack('<f', byteBuffer[idX:idX + 4])[0]
        idX += 4
        vitalsign["confidenceMetricHeartOut_xCorr"] = struct.unpack('<f', byteBuffer[idX:idX + 4])[0]
        idX += 4
        vitalsign["sumEnergyBreathWfm"] = struct.unpack('<f', byteBuffer[idX:idX + 4])[0]
        idX += 4
        vitalsign["sumEnergyHeartWfm"] = struct.unpack('<f', byteBuffer[idX:idX + 4])[0]
        idX += 4
        vitalsign["motionDetectedFlag"] = struct.unpack('<f', byteBuffer[idX:idX + 4])[0]
        idX += 4
        vitalsign["breathingRateEst_harmonicEnergy"] = struct.unpack('<f', byteBuffer[idX:idX + 4])[0]
        idX += 4
        vitalsign["heartRateEst_harmonicEnergy"] = struct.unpack('<f', byteBuffer[idX:idX + 4])[0]
        idX += 4
        vitalsign["reserved0"] = struct.unpack('<f', byteBuffer[idX:idX + 4])[0]
        idX += 4
        vitalsign["reserved1"] = struct.unpack('<f', byteBuffer[idX:idX + 4])[0]
        idX += 4
        vitalsign["reserved2"] = struct.unpack('<f', byteBuffer[idX:idX + 4])[0]
        idX += 4
        vitalsign["reserved3"] = struct.unpack('<f', byteBuffer[idX:idX + 4])[0]
        idX += 4
        vitalsign["reserved4"] = struct.unpack('<f', byteBuffer[idX:idX + 4])[0]
        idX += 4
        vitalsign["reserved5"] = struct.unpack('<f', byteBuffer[idX:idX + 4])[0]
        idX += 4
        vitalsign["reserved6"] = struct.unpack('<f', byteBuffer[idX:idX + 4])[0]
        idX += 4
        vitalsign["reserved7"] = struct.unpack('<f', byteBuffer[idX:idX + 4])[0]
        idX += 4
        vitalsign["tlv2_type_"] = int.from_bytes(byteBuffer[idX:idX + 4], byteorder='little')
        idX += 4
        vitalsign["tlv2_length_"] = int.from_bytes(byteBuffer[idX:idX + 4], byteorder='little')
        idX += 4
        
        numRangeBinProcessed = vitalsign["rangeBinEndIndex"]-vitalsign["rangeBinStartIndex"]+1
        
        range_profile = "["
        for i in range(numRangeBinProcessed):
            RPrealpart = int.from_bytes(byteBuffer[idX:idX + 2], 'little')
            idX += 2
            RPimagelpart = int.from_bytes(byteBuffer[idX:idX + 2], 'little')
            idX += 2

            range_profile += " " + str(pow(RPrealpart*RPrealpart+RPimagelpart*RPimagelpart,0.5))
        
        range_profile += "]"
        
        vitalsign["RangeProfile"] = str(range_profile)
        
        return vitalsign

    def run(self):
        try:
            if self.__serialConfig():
                self.__startDevice = True
                self.__start_thread = threading.Thread(target= self.__start)
                self.__start_thread.start()
                print('Radar Started')
        except Exception as e:
            self.__logger.error(f"Error running Radar_IWR6843: {e}")

    def stopRadar(self):
        try:
            self.__startDevice = False
            self.__start_thread.join()
            self.__CLIport.write(('resetDevice\n').encode())
            self.__CLIport.write(('sensorStop\n').encode())
            self.__CLIport.close()
            self.__Dataport.close()
            print('RADAR Stopped!')
        except Exception as e:
            self.__logger.error(f"Error stopping radar: {e}")

    def __start(self):
        while self.__startDevice:
            try:
                if self.__Dataport.in_waiting <= 0:
                    continue

                headerData = self.__readHeader()
                vitalsign = self.__ParseHeader(headerData)
                packet_len = int(vitalsign["totalPacketLen"])
                bodyData = self.__readPacketBody(packet_len)
                vitalsigns = self.__ParsePacketBody(vitalsign, bodyData)
                data = {'Data': vitalsigns, 'Time': datetime.now().strftime('%F %T.%f')[:-3] }

                if not self.__get_data_func == None:
                    self.__get_data_func(data)

            except Exception as e:
                self.__logger.error(f"Error getting vital signs: {e}")