class Broadcaster():
    def __init__(self) -> None:
        self.__subscribers = []
    
    def subscribe(self, func):
        def unsubscribe():
            if func in self.__subscribers:
                self.__subscribers.remove(func)
        if func not in self.__subscribers:
            self.__subscribers.append(func)
        return unsubscribe
    
    def broadcast(self, data):
        [observer(data) for observer in self.__subscribers]