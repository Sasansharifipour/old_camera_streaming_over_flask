import logging
import cv2
from threading import Thread
import k4a


class Azure_Kinect_DK_Connector:

    def __init__(self, get_capture_func= None, get_color_func= None, get_depth_func= None, get_ir_func= None):
        try:
            self.__get_capture_func=    get_capture_func
            self.__get_color_func=      get_color_func
            self.__get_depth_func=      get_depth_func
            self.__get_ir_func=         get_ir_func

            self.__device = k4a.Device.open()
                        
            if self.__device is None:
                raise IOError('Can not connect to Azure Kinect DK device')
            
            self.__device_config = k4a.DeviceConfiguration(
                color_format=k4a.EImageFormat.COLOR_BGRA32,
                color_resolution=k4a.EColorResolution.RES_1080P,
                depth_mode=k4a.EDepthMode.WFOV_2X2BINNED, # k4a.EDepthMode.WFOV_UNBINNED,
                camera_fps=k4a.EFramesPerSecond.FPS_30,
                synchronized_images_only=True,
                depth_delay_off_color_usec=0,
                wired_sync_mode=k4a.EWiredSyncMode.STANDALONE,
                subordinate_delay_off_master_usec=0,
                disable_streaming_indicator=False)
        
            self.__startDevice = False
        except Exception as e:
            logging.error(f"Error initializing Azure Kinect DK Connector: {str(e)}")

    def start(self):
        if not self.__startDevice:
            status = self.__device.start_cameras(self.__device_config)

            if status != k4a.EStatus.SUCCEEDED:
                raise IOError("Can't open the Azure Kinect DK device")
            
            self.__startDevice = True
            self.__recording = Thread(target=self.__startRecording)
            self.__recording.start()

    def __startRecording(self):
        try:
            while self.__startDevice:
                capture = self.__device.get_capture(1)

                if (capture == None):
                    continue

                if not self.__get_capture_func == None:
                    self.__get_capture_func(capture)
                
                if not self.__get_color_func == None:
                    self.__get_color_func({'Time': capture.color.system_timestamp_nsec, 'Data': capture.color.data})
                    
                if not self.__get_depth_func == None:
                    self.__get_depth_func({ 'Time': capture.depth.system_timestamp_nsec, 'Data': cv2.applyColorMap(cv2.convertScaleAbs(capture.depth.data, alpha=0.05), cv2.COLORMAP_JET)})
                    
                if not self.__get_ir_func == None:
                    self.__get_ir_func({'Time': capture.ir.system_timestamp_nsec, 'Data': cv2.convertScaleAbs(capture.ir.data, alpha=0.5)})

        except Exception as e:
            logging.error(f"Error in startRecording: {str(e)}")

    def stop(self):
        self.__startDevice = False
        self.__recording.join()
        self.__device.stop_cameras()
        print('Camera Stopped!!!')
