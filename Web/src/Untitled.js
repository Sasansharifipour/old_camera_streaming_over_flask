var breathingRates = []
var heartRates = []

var chartColors = {
	red: 'rgb(255, 99, 132)',
	orange: 'rgb(255, 159, 64)',
	yellow: 'rgb(255, 205, 86)',
	green: 'rgb(75, 192, 192)',
	blue: 'rgb(54, 162, 235)',
	purple: 'rgb(153, 102, 255)',
	grey: 'rgb(201, 203, 207)'
};

function onRefreshBreathingRateChart(chart) {
	chart.config.data.datasets.forEach(function(dataset) {
		dataset.data.push({
			x: Date.now(),
			y: breathingRates.pop()
		});
	});
}

function onRefreshHeartRateChart(chart) {
	chart.config.data.datasets.forEach(function(dataset) {
		dataset.data.push({
			x: Date.now(),
			y: heartRates.pop()
		});
	});
}

Chart.defaults.global.legend.display = false;
var color = Chart.helpers.color;

var configBreathingRateChart = {
	type: 'line',
	data: {
        datasets: [{
            borderColor: chartColors.green,
            data: []
      }]
    },
	options: {
		scales: {
			xAxes: [{
				type: 'realtime'
			}],
			yAxes: [{
                ticks: {
                  stepSize: 1
                }
              }]
		},
		tooltips: {
			mode: 'nearest',
			intersect: false
		},
		hover: {
			mode: 'nearest',
			intersect: false
		},
        plugins: {
            streaming: {
                refresh: 50,
              onRefresh: onRefreshBreathingRateChart
            }
          },
    pan: {
        enabled: true,    // Enable panning
        mode: 'x',        // Allow panning in the x direction
        rangeMin: {
            x: null       // Min value of the delay option
        },
        rangeMax: {
            x: null       // Max value of the delay option
        }
    },
    zoom: {
        enabled: true,    // Enable zooming
        mode: 'x',        // Allow zooming in the x direction
        rangeMin: {
            x: null       // Min value of the duration option
        },
        rangeMax: {
            x: null       // Max value of the duration option
        }
    }
  }
};

var configHeartRateChart = {
	type: 'line',
	data: {
        datasets: [{
            borderColor: chartColors.red,
            data: []
      }]
    },
	options: {
		scales: {
			xAxes: [{
				type: 'realtime'
			}],
			yAxes: [{
                title: 'Heart Rate',
                ticks: {
                  stepSize: 1
                }
              }]
		},
		tooltips: {
			mode: 'nearest',
			intersect: false
		},
		hover: {
			mode: 'nearest',
			intersect: false
		},
        plugins: {
            streaming: {
                refresh: 50,
              onRefresh: onRefreshHeartRateChart
            }
          },
    pan: {
        enabled: true,    // Enable panning
        mode: 'x',        // Allow panning in the x direction
        rangeMin: {
            x: null       // Min value of the delay option
        },
        rangeMax: {
            x: null       // Max value of the delay option
        }
    },
    zoom: {
        enabled: true,    // Enable zooming
        mode: 'x',        // Allow zooming in the x direction
        rangeMin: {
            x: null       // Min value of the duration option
        },
        rangeMax: {
            x: null       // Max value of the duration option
        }
    }
  }
};

window.onload = function() {
	var ctx = document.getElementById('breathingRateChart').getContext('2d');
	window.breathingRateChart = new Chart(ctx, configBreathingRateChart);

    
	var heart_ctx = document.getElementById('heartRateChart').getContext('2d');
	window.heartRateChart = new Chart(heart_ctx, configHeartRateChart);
};

var colorNames = Object.keys(chartColors);

function receive_data_from_server(data){
    const obj = JSON.parse(data.data);
    //$("#data").text(data.data);
    breathingRates.push(obj.outputFilterBreathOut)
    heartRates.push(obj.outputFilterHeartOut)
}