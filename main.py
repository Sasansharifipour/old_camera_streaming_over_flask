import argparse
import logging
from time import sleep
import json
from Real_Sense.Real_Sense_Connector import Real_Sense_Connector
import cv2
import timeit
import signal
from typing import List
from Adapters.ConnectorAdapter import ConnectorAdapter
from Common.Observer import Observer
from Factory import Factory
from flask import Flask, Response, redirect, url_for, request, render_template

app = Flask(__name__, template_folder='Web/Pages', static_folder='Web/src')

factory = Factory()
color_Observer, camera_rgb_broadcaster = factory.Create()
depth_Observer, depth_rgb_broadcaster = factory.Create()

camera: Real_Sense_Connector = Real_Sense_Connector(get_color_func= camera_rgb_broadcaster.broadcast, get_depth_func= depth_rgb_broadcaster.broadcast)
cameraAdapter: ConnectorAdapter = ConnectorAdapter(device_name= 'Azure Kinect DK Color Image', device_start_func= camera.start, device_stop_func= camera.stop)

connector_adapters : List[ConnectorAdapter] = [cameraAdapter]
[connector_adapter.start() for connector_adapter in connector_adapters]

vital_signs_default_frameRate = 20
video_default_frameRate = 30

logging.basicConfig(filename='error.log', level=logging.ERROR)

def Image_reader(frameRate, observer):
        try:
            while True:
                observer.event.wait()
                startTime = timeit.default_timer()
                flag, frame = cv2.imencode(".jpg", observer.data['Data'])
                if not flag:
                     continue
                frame_bytes = frame.tobytes() #bytes(frame)
                observer.event.clear()
                
                if not flag:
                    continue
                
                yield (b'--frame\r\nContent-type:image/jpeg\r\nContent-length:' + f"{len(frame_bytes)}".encode() + b'\r\n\r\n' + frame_bytes + b'\r\n')
                
                # yield(b'--frame\r\n Content-Type: image/jpeg\r\n\r\n' + bytearray(frame) + b'\r\n')
                elapsed = timeit.default_timer() - startTime
                sleep(max(0, (1/frameRate) - elapsed))
                
        except GeneratorExit:
            return

def vital_signs_reader(frameRate, observer):
        try:
            while True:
                observer.event.wait()
                startTime = timeit.default_timer()
                json_string = json.dumps(observer.data['Data'], ensure_ascii=False)
                observer.event.clear()
                yield "data: %s \n\n" % json_string
                elapsed = timeit.default_timer() - startTime
                sleep(max(0, (1/frameRate) - elapsed))
          
        except GeneratorExit:
            return

@app.route("/")
def Index():
	return render_template('index.html')

@app.route("/show_vital")
def show_vital():
	return render_template('vital_signs.html')

url_dict = [{"url"  : "/", 
             "type" : "index"},
            {"url"  : "/color_image", 
             "type" : "video"},
            {"url"  : "/depth_image", 
             "type" : "video"},
            {"url"  : "/info", 
             "type" : "info"}]

@app.route("/info")
def Info():
	return json.dumps(url_dict, ensure_ascii=False)

@app.route('/depth_image')
def depth_image():
    return Response(Image_reader(getFrameRate(video_default_frameRate), depth_Observer), mimetype = "multipart/x-mixed-replace; boundary=--frame")

@app.route('/color_image')
def color_image():
    return Response(Image_reader(getFrameRate(video_default_frameRate), color_Observer), mimetype = "multipart/x-mixed-replace; boundary=--frame")

def getFrameRate(defaultFrameRate = 30):
     try:
         if ('frameRate' in request.args):
              return int(request.args.get('frameRate'))
         elif ('framerate' in request.args):
              return int(request.args.get('framerate'))
         else:
              return defaultFrameRate
     except:
         return defaultFrameRate

def handler(signum, frame):
    [connector_adapter.stop() for connector_adapter in connector_adapters]
    exit()

signal.signal(signal.SIGBREAK, handler)

def read_params():
    ap = argparse.ArgumentParser()
    ap.add_argument("-IP", "--ip", required=False, default="127.0.0.1", help="IP of this server on the network")
    ap.add_argument("-PORT", "--port", required=False, default="5000", help="PORT for running the Flask server")
    args = vars(ap.parse_args())
    return args

if __name__ == '__main__':
     print('Press CTRL+Break to exit')
     args = read_params()
     
     ip = args['ip']
     port = args['port']

     app.run(host=ip, port=port, debug=False, threaded=True, use_reloader=False)