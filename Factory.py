 
from enum import Enum
import os
from typing import List
from Broadcaster.Broadcaster import Broadcaster

from Common.Observer import  Observer
from pathlib import Path

class DeviceType(Enum):
    RADAR = 1
    CAMERA_RGB = 2
    CAMERA_DEPTH = 3
    CAMERA_IR = 4

class Factory():

    def Create(self):

        broadcaster: Broadcaster = Broadcaster()
        observer: Observer= Observer()
        
        broadcaster.subscribe(observer.receive_data)

        return observer, broadcaster
