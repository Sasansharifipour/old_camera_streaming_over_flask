import threading
import logging

class Observer():
    def __init__(self):
        self.event = threading.Event()

    def receive_data(self, data):
        try:
            self.data = data
            self.event.set()
        except Exception as e:
            logging.error(f"Error receiving data: {e}")