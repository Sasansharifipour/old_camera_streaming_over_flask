import logging
import cv2
from threading import Thread
import pyrealsense2 as rs
import numpy as np
from datetime import datetime


class Real_Sense_Connector:

    def __init__(self, get_capture_func= None, get_color_func= None, get_depth_func= None):
        try:
            self.__get_capture_func=    get_capture_func
            self.__get_color_func=      get_color_func
            self.__get_depth_func=      get_depth_func

            width=1280
            height=720

            pipeline = rs.pipeline()
            config = rs.config()

            pipeline_wrapper = rs.pipeline_wrapper(pipeline)

            if not config.can_resolve(pipeline_wrapper):
                raise IOError("No device conected!")
            
            pipeline_profile = config.resolve(pipeline_wrapper)
            device = pipeline_profile.get_device()
            device_product_line = str(device.get_info(rs.camera_info.product_line))
        
            # If the second camera is a Depth Camera
            config.enable_stream(rs.stream.depth, width, height, rs.format.z16, 30)
            config.enable_stream(rs.stream.color, width, height, rs.format.bgr8, 30)

            self.__config = config

            self.__pipeline = pipeline

            self.__startDevice = False
        except Exception as e:
            logging.error(f"Error initializing Real Sence Connector: {str(e)}")

    def start(self):
        if not self.__startDevice:
            
            self.__pipeline.start(self.__config)
            
            self.__startDevice = True
            self.__recording = Thread(target=self.__startRecording)
            self.__recording.start()

    def __startRecording(self):
        try:
            while self.__startDevice:
                    
                ## LE starts the code to align the frames
                align_to = rs.stream.color
                align = rs.align(align_to)
                ## LE ends the code
                
                # Capture the video frame by frame
                frames = self.__pipeline.wait_for_frames()

                ## LE starts the code to align the frames
                aligned_frames = align.process(frames)
                color_frame = aligned_frames.get_color_frame()
                depth_frame = aligned_frames.get_depth_frame()
                                
                # Convert images to numpy arrays
                depth_image = np.asanyarray(depth_frame.get_data())
                color_image = np.asanyarray(color_frame.get_data())
        
                # Apply colormap on depth image (image must be converted to 8-bit per pixel first)
                # depth_colormap = cv2.applyColorMap(cv2.convertScaleAbs(depth_image, alpha=0.05), cv2.COLORMAP_JET)
                # LE starts the code to map depth to color and replace the above line
                depth_color_frame = rs.colorizer().colorize(depth_frame)
                depth_colormap = np.asanyarray(depth_color_frame.get_data())
                # LE ends the code
                capture_time = datetime.now().strftime('%F %T.%f')[:-3]

                if (aligned_frames == None):
                    continue

                if not self.__get_capture_func == None:
                    self.__get_capture_func(aligned_frames)
                
                if not self.__get_color_func == None:
                    self.__get_color_func({'Time': capture_time, 'Data': color_image})
                    
                if not self.__get_depth_func == None:
                    self.__get_depth_func({ 'Time': capture_time, 'Data': depth_colormap})
                    

        except Exception as e:
            logging.error(f"Error in startRecording: {str(e)}")

    def stop(self):
        self.__startDevice = False
        self.__recording.join()
        self.__pipeline.stop()
        print('Camera Stopped!!!')
